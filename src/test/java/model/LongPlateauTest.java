package model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.*;

public class LongPlateauTest {
    LongestPlateau lp = new LongestPlateau();


    @Test
    public void testGeneratorWithoutParam(){
            Assert.assertArrayEquals(lp.generateNewArray(), lp.getArr());
    }

    @Test
    public void testGeneratorWithParameters(){
        lp.generateNewArray(1,9,10);
        int[] arr = lp.getArr();
        Assert.assertEquals(arr.length,10);
        int min = 1;
        int max = 9;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]>max) max = arr[i];
            if (arr[i]<min) min = arr[i];
        }
        Assert.assertEquals(min,1);
        Assert.assertEquals(max,9);
    }


    @RepeatedTest(value =5,name = "{displayName} {currentRepetition}/{totalRepetitions}")
    @DisplayName("PrintTest")
    public void testPrintPlateau(TestInfo testInfo){
        System.out.println( testInfo.getDisplayName());
        lp.generateNewArray();
        lp.print();

    }


}
