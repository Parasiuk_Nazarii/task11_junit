package model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MinesweeperTest {

    @InjectMocks
    Minesweeper mw = new Minesweeper(10,20,60);

    @Test
    public void testConstructor(){
        Assertions.assertEquals(mw.getHeight(),20);
        Assert.assertTrue(mw.getWidth()==10);
        Assert.assertFalse(mw.getBombCount()>140||mw.getBombCount()<100);
    }

    @RepeatedTest(100)
    public void testBombProbability(){
        mw = new Minesweeper(10,10,50);
        Assert.assertTrue(mw.getBombCount()>40&& mw.getBombCount()<60);
    }
}
